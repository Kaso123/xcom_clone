// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPointPlayer.h"

// Sets default values
ASpawnPointPlayer::ASpawnPointPlayer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnPointPlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnPointPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

