// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatGameMode.h"
#include "CombatCamera.h"
#include "CombatPlayerController.h"
#include "EnemyPawn.h"
#include "CombatCamera.h"
#include "Engine/World.h"
#include "GridManager.h"
#include "PersistentData.h"
#include "Kismet/GameplayStatics.h"

ACombatGameMode::ACombatGameMode()
{
	PlayerControllerClass = ACombatPlayerController::StaticClass();
	DefaultPawnClass = ACombatCamera::StaticClass();
}

void ACombatGameMode::BeginPlay()
{
	FindGridManager();
	m_player = Cast<ACombatCamera>(UGameplayStatics::GetActorOfClass(GetWorld(), m_CombatPlayerClass));
	m_enemy = Cast<AEnemyPawn>(UGameplayStatics::GetActorOfClass(GetWorld(), m_EnemyPawnClass));
	check(m_player != nullptr);
	check(m_enemy != nullptr);
	Super::BeginPlay();

	m_player->StartTurn(); //player starts first
	UpdateHUDInfo();
	UpdateHUDPoints();
}

void ACombatGameMode::FindGridManager()
{
	m_gridManager = Cast<AGridManager>(UGameplayStatics::GetActorOfClass(GetWorld(), m_GridManagerClass));
	check(m_gridManager != nullptr);
}

AGridManager * ACombatGameMode::GetGridManager()
{
	if (!m_gridManager)
		FindGridManager();

	check(m_gridManager != nullptr);
	
	return m_gridManager;
}

void ACombatGameMode::EndEnemyTurn()
{
	m_bPlayerTurn = true;
	m_gridManager->ClearNodes();
	m_player->StartTurn();
	UpdateHUDInfo();
	UpdateHUDPoints();
}

void ACombatGameMode::EndPlayerTurn()
{
	m_bPlayerTurn = false;
	m_gridManager->ClearNodes();
	m_enemy->StartTurn();
	UpdateHUDInfo();
	UpdateHUDPoints();
}

void ACombatGameMode::GameOver(bool bPlayerWon)
{
	check(m_player != nullptr);
	check(m_enemy != nullptr);

	m_player->SaveSoldierStatsAfterMission();
	m_enemy->SaveSoldierStatsAfterMission();

	m_enemy->EndTurn();
	m_player->EndTurn();

	Cast<UPersistentData>(GetGameInstance())->MissionFinished(bPlayerWon);
	ShowGameOverScreen(bPlayerWon);
}

FVector ACombatGameMode::GetDesiredCameraPosition()
{
	return m_enemy->GetSelectedLocation();
}

bool ACombatGameMode::UpdateHUDInfo_Implementation()
{
	return m_bPlayerTurn;
}

FString ACombatGameMode::ShowMessageOnScreen_Implementation(const FString& msg)
{
	return msg;
}

void ACombatGameMode::BackToBase()
{
	GetWorld()->ServerTravel(FString("/Game/Maps/Base_Level"));
}

void ACombatGameMode::ShowGameOverScreen_Implementation(bool bWin)
{
}


