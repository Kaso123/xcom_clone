// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CombatPlayerController.generated.h"

/**
 * 
 */
//Used to get input from player
UCLASS()
class XCOM_CLONE_API ACombatPlayerController : public APlayerController
{
	GENERATED_BODY()

	ACombatPlayerController();

	void SetupInputComponent() override;

private:
	class ACombatCamera* m_playerPawn;
	class AGridNode* m_hoveredNode;
	class ASoldier* m_hoveredSoldier;

	void OnMouseLeftClick();
	void OnMouseRightClick();
	void OnEscape();
	void MoveCameraForwardInput(float direction);
	void MoveCameraRightInput(float direction);
	void RotateCameraRightInput(float direction);
	void ZoomCameraInput(float direction);
	void StartSoldierHover(ASoldier* soldier);
	void EndSoldierHover();
	virtual void Tick(float deltaTime) override;
	virtual void BeginPlay() override;
};
