// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatPlayerController.h"
#include "GridNode.h"
#include "SelectionCircle.h"
#include "CombatCamera.h"
#include "Engine/World.h"
#include "Soldier.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"


ACombatPlayerController::ACombatPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
}

void ACombatPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("MouseLeftClick", IE_Pressed, this, &ACombatPlayerController::OnMouseLeftClick);
	InputComponent->BindAction("MouseRightClick", IE_Pressed, this, &ACombatPlayerController::OnMouseRightClick);
	InputComponent->BindAction("Escape", IE_Pressed, this, &ACombatPlayerController::OnEscape);
	// keyboard move (WASD, Home/End)
	InputComponent->BindAxis("MoveForward", this, &ACombatPlayerController::MoveCameraForwardInput);
	InputComponent->BindAxis("MoveRight", this, &ACombatPlayerController::MoveCameraRightInput);
	InputComponent->BindAxis("RotateRight", this, &ACombatPlayerController::RotateCameraRightInput);
	InputComponent->BindAxis("ZoomIn", this, &ACombatPlayerController::ZoomCameraInput);
}

void ACombatPlayerController::BeginPlay()
{
	Super::BeginPlay();
	m_playerPawn = Cast<ACombatCamera>(GetPawn());
	check(m_playerPawn != nullptr);
}


void ACombatPlayerController::Tick(float deltaTime)
{
	FHitResult hit(ForceInit);
	GetHitResultUnderCursor(ECC_WorldStatic, false, hit);

	if (hit.GetActor()) //mouse over something
	{		
		AGridNode* hitGridNode = Cast<AGridNode>(hit.GetActor());
		if (hitGridNode) //mouse over node
		{
			if (hitGridNode != m_hoveredNode) //only do this if hovered node is not already selected in GUI
			{
				if (m_hoveredNode)
					m_hoveredNode->MouseOverNodeEnd();
				if (m_hoveredNode && !m_hoveredNode->CanSelectedWalkHere())
					m_hoveredNode->HideNode();

				m_hoveredNode = hitGridNode;
				m_hoveredNode->MouseOverNode();	
			}		
		}
		else //no node is under mouse cursor, hide previously selected node
		{
			if (m_hoveredNode)
				m_hoveredNode->MouseOverNodeEnd();
			if (m_hoveredNode && !m_hoveredNode->CanSelectedWalkHere())
				m_hoveredNode->HideNode();
		}

		ASoldier* hitSoldier = Cast<ASoldier>(hit.GetActor());
		if (hitSoldier) //mouse over soldier, show info in GUI
		{
			if (m_hoveredSoldier != hitSoldier)
			{
				StartSoldierHover(hitSoldier);
			}	
		}
		else //hide info when not hovering over soldier
		{
			EndSoldierHover();
		}
	}
	
}


void ACombatPlayerController::OnMouseLeftClick()
{	
	FHitResult hit(ForceInit);
	GetHitResultUnderCursor(ECC_Pawn, false, hit);

	if (hit.GetActor()) //only selecting soldiers, nodes do nothing on left click
	{
		ASoldier* hitSoldier = Cast<ASoldier>(hit.GetActor());
		if (hitSoldier && !hitSoldier->IsEnemy())
		{
			m_playerPawn->Select(hitSoldier);
		}
			
	}
}

void ACombatPlayerController::OnMouseRightClick()
{
	FHitResult hit(ForceInit);
	GetHitResultUnderCursor(ECC_WorldStatic, false, hit);

	if (hit.GetActor())
	{
		AGridNode* hitGridNode = Cast<AGridNode>(hit.GetActor());
		if (hitGridNode) //right click on node
		{
			m_playerPawn->MoveToNode(hitGridNode);
			return;
		}
		ASoldier* hitSoldier = Cast<ASoldier>(hit.GetActor());
		if (hitSoldier) //right click on soldier
		{
			m_playerPawn->Attack(hitSoldier);
			return;
		}	
	}
}

void ACombatPlayerController::OnEscape()
{
	m_playerPawn->LeaveMission();
}



void ACombatPlayerController::MoveCameraForwardInput(float direction)
{
	m_playerPawn->MoveCameraForwardInput(direction);
}

void ACombatPlayerController::MoveCameraRightInput(float direction)
{
	m_playerPawn->MoveCameraRightInput(direction);
}

void ACombatPlayerController::RotateCameraRightInput(float direction)
{
	m_playerPawn->RotateCameraRightInput(direction);
}

void ACombatPlayerController::ZoomCameraInput(float direction)
{
	m_playerPawn->ZoomCameraInput(direction);
}

void ACombatPlayerController::StartSoldierHover(ASoldier * soldier)
{

	if (m_hoveredSoldier && m_hoveredSoldier != soldier) //something was hovered before
	{
		m_hoveredSoldier->HideHoverGUI();
		m_hoveredSoldier->HideRangeGUI();
		m_hoveredSoldier = soldier;
		m_hoveredSoldier->ShowHoverGUI();
		m_hoveredSoldier->ShowRangeGUI();

	}
	else if (m_hoveredSoldier != soldier) //nothing hovered before
	{
		m_hoveredSoldier = soldier;
		m_hoveredSoldier->ShowHoverGUI();
		m_hoveredSoldier->ShowRangeGUI();
	}
}

void ACombatPlayerController::EndSoldierHover()
{
	if (!m_hoveredSoldier)
		return;

	m_hoveredSoldier->HideRangeGUI();
	m_hoveredSoldier->HideHoverGUI();
	m_hoveredSoldier = nullptr;
}
