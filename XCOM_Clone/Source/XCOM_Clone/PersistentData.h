// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Soldier.h"
#include "Engine/GameInstance.h"
#include "PersistentData.generated.h"

//used for saving data between levels and main screen
/**
 * 
 */
UCLASS()
class XCOM_CLONE_API UPersistentData : public UGameInstance
{
	GENERATED_BODY()

	UPersistentData();

private:

	TArray<FSoldierInfo> m_soldiersList;
	TArray<FSoldierInfo> m_selectedSoldiers;
	int m_maxId = 0;

	int m_missionsSuccessful = 0;
	int m_missionsFailed = 0;
	int m_soldiersLost = 0;
	int m_soldiersKilled = 0;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Images")
	UTexture2D* m_infantryTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Images")
	UTexture2D* m_sniperTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Images")
	UTexture2D* m_medicTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Images")
	UTexture2D* m_heavyTexture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	int m_desiredEnemyCount;

	
	TArray<FSoldierInfo>& GetSoldiersList() { return m_soldiersList; };

	TArray<FSoldierInfo>& GetSelectedList() { return m_selectedSoldiers; };

	TArray<FSoldierInfo> GetEnemiesList();

	void FillData();

	UFUNCTION(BlueprintCallable)
	void SoldierSelected(FSoldierInfo soldier);

	UFUNCTION(BlueprintCallable)
	void SoldierDeselected(FSoldierInfo soldier);
	void PrintSelected();

	void UpdateSoldiers(const TArray<ASoldier*>& list);
	void MissionFinished(bool win);
	void SoldierKilled() { m_soldiersKilled++; };
	void SoldierLost(FSoldierInfo info); 

	UFUNCTION(BlueprintCallable)
	FString GetSuccessfulMissionsString() { return FString::Printf(TEXT("Successful missions: %d"), m_missionsSuccessful); };
	UFUNCTION(BlueprintCallable)
	FString GetFailedMissionsString() { return FString::Printf(TEXT("Failed missions: %d"), m_missionsFailed); };
	UFUNCTION(BlueprintCallable)
	FString GetLostSoldiersString() { return FString::Printf(TEXT("Soldiers lost: %d"), m_soldiersLost); };
	UFUNCTION(BlueprintCallable)
	FString GetKilledSoldiersString() { return FString::Printf(TEXT("Enemies killed: %d"), m_soldiersKilled); };

	UFUNCTION(BlueprintCallable)
	FString GetStateEnumString(ESoldierStates state);
	UFUNCTION(BlueprintCallable)
	FString GetClassEnumString(ESoldierTypes type);
};
