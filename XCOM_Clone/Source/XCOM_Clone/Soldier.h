// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Soldier.generated.h"


UENUM(BlueprintType)
enum class ESoldierTypes:uint8
{
	Empty UMETA(DisplayName = ""),
	Infantry UMETA(DisplayName = "Infantry"),
	Heavy UMETA(DisplayName = "Heavy"),
	Medic UMETA(DisplayName = "Medic"),
	Sniper UMETA(DisplayName = "Sniper")
	
};

UENUM(BlueprintType)
enum class ESoldierStates :uint8
{
	Empty UMETA(DisplayName = ""),
	Ready UMETA(DisplayName = "Ready"),
	Injured UMETA(DisplayName = "Injured"),
	Dead UMETA(DisplayName = "Dead")
};

USTRUCT(BlueprintType)
struct FSoldierInfo {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		int id;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		FString m_name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		ESoldierTypes m_type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		ESoldierStates m_status;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
		UTexture2D* m_image = nullptr;

	FORCEINLINE bool operator==(const FSoldierInfo &other) const
	{
		return id == other.id;
	}
};

//getting strings to show in GUI for various enums
namespace soldierEnumToString {

	UFUNCTION(BlueprintCallable)
	FString GetTypeEnumString(ESoldierTypes type);

	UFUNCTION(BlueprintCallable)
	FString GetStateEnumString(ESoldierStates state);
}


//this class is used by player and enemy on all classes for every soldier on battlefield
UCLASS()
class XCOM_CLONE_API ASoldier : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASoldier();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class AAIController* m_aiCtrl;
	class AGridNode* m_currentNode;
	class AGridManager* m_gridManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	FSoldierInfo m_info;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	float m_currentSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool m_bIsShooting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		bool m_bIsMoving = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool m_bIsDying = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool m_bIsHit = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsHealth = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsMaxHealth = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsAim = 75;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsRange = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsDamage = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsMoveCost = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int m_statsShotCost = 5;

	bool m_bIsEnemy = false;

	bool GetRandomHitChance(ASoldier* target);
	
	int m_moveStuckCounter = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Fixes")
	int m_maxMoveStuckCounter = 200;

	void TeleportToTarget();

	void ShowAttackStatsOnScreen(int chance, int cover, bool hit);
	void StartActionInInstigator();
	void FinishActionInInstigator();
	bool IsCloseToTargetNode();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Select(int pointsLeft, bool bEnemy = false);

	void StartMoving(class AGridNode* node,int pointsLeft);
	void SetSpeed(float speed);
	void TakeDmg(int dmg);
	void Die();
	void Shoot(ASoldier* target, int pointsLeft);
	void LowerHealth();

	class AGridNode* GetNode();

	bool IsFriendly(ASoldier* soldier);
	bool IsInRange(ASoldier* soldier);
	bool IsInRange(class AGridNode* target);
	void SetIsEnemy(bool value) { m_bIsEnemy = value; }

	int GetCoverValue(ASoldier* target);
	int GetCoverValue(class AGridNode* target);

	UFUNCTION(BlueprintCallable)
	bool IsShooting() { return m_bIsShooting; };
	UFUNCTION(BlueprintCallable)
	bool IsDying() { return m_bIsDying; };
	UFUNCTION(BlueprintCallable)
	bool IsHit() { return m_bIsHit; };

	bool IsEnemy() { return m_bIsEnemy; };

	UFUNCTION(BlueprintCallable)
	void StopShootingAnim();

	UFUNCTION(BlueprintCallable)
	void StopHitAnim();
	
	UFUNCTION(BlueprintCallable)
	float GetSpeed() { return m_currentSpeed; };
	
	float GetMoveCost() { return m_statsMoveCost; };
	float GetShotCost() { return m_statsShotCost; };
	int GetRange() { return m_statsRange; };

	UFUNCTION(BlueprintCallable)
	float GetHealthPercentage() { return m_statsHealth / (float)m_statsMaxHealth; };

	UFUNCTION(BlueprintCallable)
	int GetHealthNumber() { return m_statsHealth; };

	void SetInfo(FSoldierInfo info) { m_info = info; };
	FSoldierInfo& GetInfo() { return m_info; };

	UFUNCTION(BlueprintCallable)
	FString GetName() { return m_info.m_name; };

	UFUNCTION(BlueprintCallable)
	FString GetHealthString() { return FString::Printf(TEXT("Heatlh: %d / %d"), m_statsHealth,m_statsMaxHealth); }

	UFUNCTION(BlueprintCallable)
	FString GetClassString();

	UFUNCTION(BlueprintCallable)
	FString GetAimString() { return FString::Printf(TEXT("Aim: %d"), m_statsAim); }

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void ShowHoverGUI();

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void HideHoverGUI();

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void HideRangeGUI();

	UFUNCTION(BlueprintNativeEvent, Category = Events)
	void ShowRangeGUI();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
