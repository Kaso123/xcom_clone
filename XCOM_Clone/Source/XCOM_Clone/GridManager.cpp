// Fill out your copyright notice in the Description page of Project Settings.


#include "GridManager.h"
#include "GridNode.h"
#include "Engine/World.h"
#include "Soldier.h"
#include "LevelBorder.h"
#include "CombatGameMode.h"
#include "CombatCamera.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "GenericPlatform/GenericPlatformMath.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AGridManager::AGridManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AGridManager::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> borders; //borders are placed inside levels
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), m_levelBorderClass, borders);

	//calculate grid dimensions
	minX = 0;
	maxX = 0;
	minY = 0;
	maxY = 0;
	for (auto border : borders)
	{
		FVector loc = border->GetActorLocation();
		if (loc.X > maxX) maxX = loc.X;
		if (loc.X < minX) minX = loc.X;
		if (loc.Y > maxY) maxY = loc.Y;
		if (loc.Y < minY) minY = loc.Y;
	}
	CreateNodes();
	
}

void AGridManager::CreateNodes()
{
	//calculate how many nodes to build
	m_gridDimensions.X = (maxX - minX) / nodeSize;
	m_gridDimensions.Y = (maxY - minY) / nodeSize;

	//spawn parameters
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	//change size of array
	m_gridArray.Reserve(m_gridDimensions.X * m_gridDimensions.Y);

	//spawning nodes
	_ASSERT(node);
	int idx = 0;

	for (int i = minX; i < maxX; i += nodeSize)
	{
		for (int j = minY; j < maxY; j += nodeSize)
		{
			m_gridArray.EmplaceAt(idx, GetWorld()->SpawnActor<AGridNode>(
				m_nodeClass, FVector(i + nodeSize/2, j + nodeSize/2, 0), FRotator(0, 0, 0), spawnParams)); //spawn node on (i+50,j+50)

			m_gridArray[idx]->Init(idx / m_gridDimensions.Y, idx % (int)m_gridDimensions.Y);
			idx++;
		}
	}

	for (auto& node : m_gridArray)
	{
		CalculateCover(node);
		node->UpdateCoverGUI();
	}
}

bool AGridManager::AreCoordinatesValid(FVector2D coords)
{
	if (coords.X < 0 || coords.Y < 0)
		return false;

	if(coords.X >= m_gridDimensions.X || coords.Y >= m_gridDimensions.Y)
		return false;

	return true;
}

// Called every frame
void AGridManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//get GridNode that is closest to position
AGridNode * AGridManager::GetClosestGridNode(FVector position)
{
	float x, y;
	x = FMath::RoundHalfFromZero(position.X) - minX;
	y = FMath::RoundHalfFromZero(position.Y) - minY;

	x = x / nodeSize;
	y = y / nodeSize;

	x = FMath::RoundHalfFromZero(x)-1;
	y = FMath::RoundHalfFromZero(y)-1;

	return GetGridNode(x, y);
}

AGridNode * AGridManager::GetGridNode(int x, int y)
{
	if (AreCoordinatesValid(FVector2D(x, y)))
		return m_gridArray[x*m_gridDimensions.Y + y];

	return nullptr;
}

void AGridManager::ClearNodes()
{
	for (auto node : m_gridArray)
	{
		node->HideNode();
		node->SelectionChanged();
	}
}

AGridNode * AGridManager::GetRandomNode()
{
	AGridNode* result = nullptr;
	while (!result)
	{
		result = m_gridArray[FMath::RandRange(0, int(m_gridDimensions.X-1)) * m_gridDimensions.Y +
			FMath::RandRange(0, int(m_gridDimensions.Y-1))];
		if (!result || result->IsOccupied() || !result->IsWalkable())
			result = nullptr;
	}

	return result;
}

AGridNode * AGridManager::GetRandomNodeInRange(ASoldier * soldier, int pointsLeft)
{
	AGridNode* result = nullptr;
	FVector2D start = soldier->GetNode()->GetCoordinates();
	while (!result)
	{
		int x = FMath::RandRange(start.X - pointsLeft / soldier->GetMoveCost(), start.X + pointsLeft / soldier->GetMoveCost());
		int y = FMath::RandRange(start.Y - pointsLeft / soldier->GetMoveCost(), start.Y + pointsLeft / soldier->GetMoveCost());
		x = FMath::Clamp(x, 0, int(m_gridDimensions.X - 1));
		y = FMath::Clamp(y, 0, int(m_gridDimensions.Y - 1));

		result = m_gridArray[x * m_gridDimensions.Y + y];
		if (!result || result->IsOccupied() || !result->IsWalkable() || !result->CanSelectedWalkHere())
			result = nullptr;
	}

	return result;
}

AGridNode * AGridManager::GetBestAppeal(ASoldier * soldier, int pointsLeft)
{
	int minXGrid = FMath::Clamp((int)soldier->GetNode()->GetCoordinates().X - (int)(pointsLeft / soldier->GetMoveCost()), 0, (int)m_gridDimensions.X - 1);
	int maxXGrid = FMath::Clamp((int)soldier->GetNode()->GetCoordinates().X + (int)(pointsLeft / soldier->GetMoveCost()), 0, (int)m_gridDimensions.X - 1);
	int minYGrid = FMath::Clamp((int)soldier->GetNode()->GetCoordinates().Y - (int)(pointsLeft / soldier->GetMoveCost()), 0, (int)m_gridDimensions.Y - 1);
	int maxYGrid = FMath::Clamp((int)soldier->GetNode()->GetCoordinates().Y + (int)(pointsLeft / soldier->GetMoveCost()), 0, (int)m_gridDimensions.Y - 1);

	AGridNode* result = soldier->GetNode();
	AGridNode* temp;

	for (int i = minXGrid; i <= maxXGrid; i++)
		for (int j = minYGrid; j <= maxYGrid; j++)
		{
			temp = m_gridArray[i * m_gridDimensions.Y + j];
			if (temp->GetEnemyAppeal() > result->GetEnemyAppeal())
				result = temp;
			else if (temp->GetEnemyAppeal() == result->GetEnemyAppeal() && (result == soldier->GetNode() || FMath::RandBool()))
				result = temp;
		}
	return result;
}

AGridNode * AGridManager::GetBestAppealTowardsEnemy(ASoldier * soldier, int pointsLeft, ASoldier * target)
{
	CalcCoverAppealTowardsEnemy(soldier, target);

	return GetBestAppeal(soldier, pointsLeft);
}

void AGridManager::CalculateSoldierRange(ASoldier * soldier, int pointsLeft,bool bEnemy)
{
	ClearNodes();
	
	if (!soldier->GetNode())
		_ASSERT(0);
	
	FVector2D currentNode = soldier->GetNode()->GetCoordinates();
	int idx = currentNode.X * m_gridDimensions.Y + currentNode.Y;
	
	UE_LOG(LogTemp, Warning, TEXT("lastRecCount: %d"), debugRecursiveCount);
	debugRecursiveCount = 0;
	CalcDistanceRecursive(m_gridArray[idx],pointsLeft / soldier->GetMoveCost());

	if(bEnemy)
		CalcCoverAppeal(soldier);
}

void AGridManager::CalculateCover(AGridNode * node)
{
	check(node != nullptr);
	
	for (auto& item : node->GetCoverInfo())
	{
		item = -1;
	}

	//up,left,down,right
	FVector2D newCoords;

	newCoords = node->GetCoordinates();
	newCoords.X += 1;
	if (AreCoordinatesValid(newCoords))
	{
		if (!GetGridNode(newCoords.X, newCoords.Y)->IsWalkable())
			node->GetCoverInfo()[0] = 1;
	}

	newCoords = node->GetCoordinates();
	newCoords.Y -= 1;
	if (AreCoordinatesValid(newCoords))
	{
		if (!GetGridNode(newCoords.X, newCoords.Y)->IsWalkable())
			node->GetCoverInfo()[1] = 1;
	}

	newCoords = node->GetCoordinates();
	newCoords.X -= 1;
	if (AreCoordinatesValid(newCoords))
	{
		if (!GetGridNode(newCoords.X, newCoords.Y)->IsWalkable())
			node->GetCoverInfo()[2] = 1;
	}

	newCoords = node->GetCoordinates();
	newCoords.Y += 1;
	if (AreCoordinatesValid(newCoords))
	{
		if (!GetGridNode(newCoords.X, newCoords.Y)->IsWalkable())
			node->GetCoverInfo()[3] = 1;
	}
}

//calculate distance from selected soldier for every node in range
void AGridManager::CalcDistanceRecursive(AGridNode * node, int range, int dist /*=0*/)
{
	debugRecursiveCount++;
	//already quicker route
	if (node->GetDistance() <= dist)
		return;

	//node is not walkable
	if (dist != 0 && (!node->IsWalkable() || node->IsOccupied()))
		return;

	//set new distance for node
	node->SetDistance(dist);
	node->SetEnemyAppeal(INT_MIN);
	node->SetCanWalkHere();
	node->ShowNode();

	//check range of selected unit
	if (dist >= range)
		return;

	//go recursively to neighbours
	FVector2D coords = node->GetCoordinates();

	if(coords.X > 0)
		CalcDistanceRecursive(m_gridArray[(coords.X - 1)*m_gridDimensions.Y + coords.Y], range, dist + 1);
	if(coords.X < m_gridDimensions.X - 1)
		CalcDistanceRecursive(m_gridArray[(coords.X + 1)*m_gridDimensions.Y + coords.Y], range, dist + 1);
	if (coords.Y > 0)
		CalcDistanceRecursive(m_gridArray[coords.X * m_gridDimensions.Y + (coords.Y - 1)], range, dist + 1);
	if (coords.Y < m_gridDimensions.Y - 1)
		CalcDistanceRecursive(m_gridArray[coords.X*m_gridDimensions.Y + (coords.Y + 1)], range, dist + 1);
}

void AGridManager::CalcCoverAppeal(ASoldier * soldier)
{
	const TArray<ASoldier*>& playerSoldiers = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetPlayer()->GetSoldiersList();

	for (auto& node : m_gridArray)
	{
		if (!node->CanSelectedWalkHere())
			continue;

		int value = 0;
		for (auto attacker : playerSoldiers)
		{
			if (attacker->IsInRange(node))
			{
				if(attacker->GetCoverValue(node) <= 0) //if someone can attack on this node it is not good
					value -= 1;
				else
					value += 1;
			}
		}
		node->SetEnemyAppeal(value);
		//UE_LOG(LogTemp, Warning, TEXT("[%d,%d] c: %d"), int(node->GetCoordinates().X), int(node->GetCoordinates().Y), value);
		//UKismetSystemLibrary::DrawDebugString(GetWorld(), node->GetActorLocation(), FString::Printf(TEXT("%d"),value), nullptr, FLinearColor::Blue, 25.0f);

	}
}

void AGridManager::CalcCoverAppealTowardsEnemy(ASoldier * soldier, ASoldier * target)
{
	for (auto& node : m_gridArray)
	{
		if (!node->CanSelectedWalkHere())
			continue;

		int value = node->GetEnemyAppeal();
		if (value == INT_MIN)
			continue;

		//value *= 2;
		value -= int(FGenericPlatformMath::Sqrt(
			(
			(FMath::Abs(target->GetNode()->GetCoordinates().X - node->GetCoordinates().X))
			*
			(FMath::Abs(target->GetNode()->GetCoordinates().X - node->GetCoordinates().X))
			)
			+
			(
			(FMath::Abs(target->GetNode()->GetCoordinates().Y - node->GetCoordinates().Y))
			*
			(FMath::Abs(target->GetNode()->GetCoordinates().Y - node->GetCoordinates().Y))
			)
		));

		node->SetEnemyAppeal(value);
		//UE_LOG(LogTemp, Warning, TEXT("[%d,%d] c_e: %d"), int(node->GetCoordinates().X), int(node->GetCoordinates().Y), value);
		//UKismetSystemLibrary::DrawDebugString(GetWorld(), FVector(0.0f,0.0f,10.0f), FString::Printf(TEXT("%d"),value), node, FLinearColor::Blue, 25.0f);
	}
}

AGridNode * AGridManager::GetBestAppealRecursive(AGridNode * node, int range)
{
	AGridNode* result = node;

	if (node->GetDistance() > range)
		return result;

	//go recursively to neighbours
	FVector2D coords = node->GetCoordinates();

	if (coords.X > 0)
	{
		AGridNode* temp = m_gridArray[(coords.X - 1)*m_gridDimensions.Y + coords.Y];
		if (node->GetDistance() < temp->GetDistance() && !temp->IsOccupied() && temp->IsWalkable() && temp->CanSelectedWalkHere())
		{
			temp = GetBestAppealRecursive(temp, range);
			if (temp->GetEnemyAppeal() > node->GetEnemyAppeal())
				result = temp;
			else if (temp->GetEnemyAppeal() == node->GetEnemyAppeal() && FMath::RandBool())
				result = temp;
		}
			
	}		
	if (coords.X < m_gridDimensions.X - 1)
	{
		AGridNode* temp = m_gridArray[(coords.X + 1)*m_gridDimensions.Y + coords.Y];
		if (node->GetDistance() < temp->GetDistance() && !temp->IsOccupied() && temp->IsWalkable() && temp->CanSelectedWalkHere())
		{
			temp = GetBestAppealRecursive(temp, range);
			if (temp->GetEnemyAppeal() > node->GetEnemyAppeal())
				result = temp;
			else if (temp->GetEnemyAppeal() == node->GetEnemyAppeal() && FMath::RandBool())
				result = temp;
		}
	}
	if (coords.Y > 0)
	{
		AGridNode* temp = m_gridArray[coords.X * m_gridDimensions.Y + (coords.Y - 1)];
		if (node->GetDistance() < temp->GetDistance() && !temp->IsOccupied() && temp->IsWalkable() && temp->CanSelectedWalkHere())
		{
			temp = GetBestAppealRecursive(temp, range);
			if (temp->GetEnemyAppeal() > node->GetEnemyAppeal())
				result = temp;
			else if (temp->GetEnemyAppeal() == node->GetEnemyAppeal() && FMath::RandBool())
				result = temp;
		}
	}	
	if (coords.Y < m_gridDimensions.Y - 1)
	{
		AGridNode* temp = m_gridArray[coords.X*m_gridDimensions.Y + (coords.Y + 1)];
		if (node->GetDistance() < temp->GetDistance() && !temp->IsOccupied() && temp->IsWalkable() && temp->CanSelectedWalkHere())
		{
			temp = GetBestAppealRecursive(temp, range);
			if (temp->GetEnemyAppeal() > node->GetEnemyAppeal())
				result = temp;
			else if (temp->GetEnemyAppeal() == node->GetEnemyAppeal() && FMath::RandBool())
				result = temp;
		}
	}		
	debugRecursiveCount++;
	return result;
}

