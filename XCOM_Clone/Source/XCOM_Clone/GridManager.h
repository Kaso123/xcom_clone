// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridManager.generated.h"


//Used for spawning grid, calculating stuff using grid
UCLASS()
class XCOM_CLONE_API AGridManager : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AGridManager();

protected:

	UPROPERTY(EditAnywhere, Category = "Grid") 
	TSubclassOf<class AGridNode> m_nodeClass; //Grid node BP

	UPROPERTY(EditAnywhere, Category = "Grid")
	TSubclassOf<class ALevelBorder> m_levelBorderClass; //LevelBorder BP

	//level boundaries
	int minX;
	int maxX;
	int minY;
	int maxY;

	UPROPERTY(EditAnywhere, Category = "Grid")
	int nodeSize = 100;

	TArray<class AGridNode*> m_gridArray;
	FVector2D m_gridDimensions; //size of array

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void CreateNodes();
	bool AreCoordinatesValid(FVector2D coords); //checks if node with this coordinates exists

	int debugRecursiveCount;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	class AGridNode* GetClosestGridNode(FVector position); //get closest grid node to float position
	class AGridNode* GetGridNode(int x, int y);
	void ClearNodes(); //hide all nodes

	class AGridNode* GetRandomNode();
	class AGridNode* GetRandomNodeInRange(class ASoldier* soldier, int pointsLeft); //get random node close to soldier
	class AGridNode* GetBestAppeal(class ASoldier* soldier, int pointsLeft); //get node with best appeal for soldier
	class AGridNode* GetBestAppealTowardsEnemy(class ASoldier* soldier, int pointsLeft, class ASoldier* target); //get node with best appeal when moving towards enemy target

	void CalculateSoldierRange(class ASoldier* soldier, int pointLeft,bool bEnemy); //calculate movement cost for nodes
	void CalculateCover(class AGridNode* node); //calculate cover from 4 sides on this node
	void CalcDistanceRecursive(class AGridNode* node, int range, int dist = 0); //used for recursive distance calculation
	void CalcCoverAppeal(class ASoldier* soldier); //calculates where it is better for soldier to move when he wants to be in cover
	void CalcCoverAppealTowardsEnemy(class ASoldier* soldier, class ASoldier* target); //calculates where to move when he wants to be in cover but going to enemy
	class AGridNode* GetBestAppealRecursive(class AGridNode* node, int range); //recursively finds best place to move in close proximity
};
