// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CombatGameMode.generated.h"


/**
 * 
 */
 //Game Mode for combat parts of game (levels), it ties everything together
UCLASS()
class XCOM_CLONE_API ACombatGameMode : public AGameModeBase
{
	GENERATED_BODY()

		ACombatGameMode();

protected:
	class AGridManager* m_gridManager = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Singletons")
	TSubclassOf<class AGridManager> m_GridManagerClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Singletons")
	TSubclassOf<class ACombatCamera> m_CombatPlayerClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Singletons")
	TSubclassOf<class AEnemyPawn> m_EnemyPawnClass;

	class ACombatCamera* m_player = nullptr;
	class AEnemyPawn* m_enemy = nullptr;
	
	bool m_bPlayerTurn = true;
	void FindGridManager();
	
	
public:
	class AGridManager* GetGridManager();

	//coordination between player and AI
	void EndEnemyTurn();
	void EndPlayerTurn();

	void GameOver(bool bPlayerWon); // bPlayerWon = true if player won the level

	FVector GetDesiredCameraPosition(); //Used to follow enemy soldiers during their turn

	UFUNCTION(BlueprintCallable)
	class AEnemyPawn* GetEnemy() { return m_enemy; };

	UFUNCTION(BlueprintCallable)
	class ACombatCamera* GetPlayer() { return m_player; };

	UFUNCTION(BlueprintCallable)
	void BackToBase();

	//these events are implemented in Blueprints, used for GUI
	UFUNCTION(BlueprintNativeEvent, Category = Event)
	bool UpdateHUDInfo();
	UFUNCTION(BlueprintNativeEvent, Category = Event)
	FString ShowMessageOnScreen(const FString& msg);
	UFUNCTION(BlueprintImplementableEvent, Category = Event)
	void UpdateHUDPoints();
	UFUNCTION(BlueprintNativeEvent, Category = Event)
	void ShowGameOverScreen(bool bWin);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
