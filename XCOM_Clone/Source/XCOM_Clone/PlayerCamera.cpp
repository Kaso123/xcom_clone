// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCamera.h"
#include "Camera/CameraComponent.h"
#include "Engine/Classes/Components/SceneComponent.h"
#include "Engine/Classes/Components/InputComponent.h"


// Sets default values
APlayerCamera::APlayerCamera(const FObjectInitializer& ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bAddDefaultMovementBindings = false; //disable WASD default

	// not needed Pitch Yaw Roll
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	m_cameraMoveSpeed = 3000.f;

	// intialize the camera
	m_cameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Overhead Camera"));
	m_cameraComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	m_cameraComponent->bUsePawnControlRotation = false;
}

void APlayerCamera::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	if (!PlayerInputComponent) return;

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// keyboard move (WASD, Home/End)
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCamera::MoveCameraForwardInput);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCamera::MoveCameraRightInput);
}
// Called when the game starts or when spawned
void APlayerCamera::BeginPlay()
{
	Super::BeginPlay();
	SetActorLocation(FVector(0.0f, 0.0f, 0.0f));
	
}

// Called every frame
void APlayerCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector currLocation = GetActorLocation();
	FVector movement = FVector::ZeroVector;

	FRotator cameraYaw = FRotator(0.0f, m_cameraComponent->GetComponentToWorld().Rotator().Yaw, 0.0f);

	movement += m_forwardSpeed * m_cameraMoveSpeed * DeltaTime * cameraYaw.Vector();
	movement += m_rightSpeed * m_cameraMoveSpeed * DeltaTime * (FRotator(0.f,90.f,0.f) + cameraYaw).Vector();
	currLocation += movement;
	SetActorLocation(currLocation);
}

void APlayerCamera::MoveCameraForwardInput(float direction)
{
	if(direction > 0)UE_LOG(LogTemp, Warning, TEXT("move forward %f"), direction);
	m_forwardSpeed = direction;
	//m_cameraComponent->SetRelativeLocation(FVector(direction*1000, 0.f, 0.f));
}

void APlayerCamera::MoveCameraRightInput(float direction)
{
	m_rightSpeed = direction;
	//	m_cameraComponent->SetRelativeLocation(FVector(0.f, direction*1000, 0.f));
}

