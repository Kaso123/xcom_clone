// Fill out your copyright notice in the Description page of Project Settings.


#include "PersistentData.h"


UPersistentData::UPersistentData() {

	FSoldierInfo newSoldier;
	newSoldier.m_name = "John Locke";
	newSoldier.m_status = ESoldierStates::Injured;
	newSoldier.m_type = ESoldierTypes::Infantry;
	newSoldier.id = m_maxId++;

	FSoldierInfo newSoldier2;
	newSoldier2.m_name = "Jack Shepard";
	newSoldier2.m_status = ESoldierStates::Ready;
	newSoldier2.m_type = ESoldierTypes::Sniper;
	newSoldier2.id = m_maxId++;

	FSoldierInfo newSoldier3;
	newSoldier3.m_name = "Hugo Reyes";
	newSoldier3.m_status = ESoldierStates::Ready;
	newSoldier3.m_type = ESoldierTypes::Heavy;
	newSoldier3.id = m_maxId++;

	FSoldierInfo newSoldier4;
	newSoldier4.m_name = "Kate Austen";
	newSoldier4.m_status = ESoldierStates::Ready;
	newSoldier4.m_type = ESoldierTypes::Medic;
	newSoldier4.id = m_maxId++;

	m_soldiersList.Init(newSoldier,1);
	m_soldiersList.Add(newSoldier2);
	m_soldiersList.Add(newSoldier3);
	m_soldiersList.Add(newSoldier4);

	m_desiredEnemyCount = 4;
}

TArray<FSoldierInfo> UPersistentData::GetEnemiesList()
{
	TArray<FSoldierInfo> arr;

	for (int i = 0; i < m_desiredEnemyCount; i++)
	{
		FSoldierInfo info;
		info.id = m_maxId++;
		info.m_status = ESoldierStates::Ready;
		info.m_type = ESoldierTypes::Infantry;
		info.m_name = "Enemy Infantry";

		arr.Add(info);
	}

	return arr;
}

void UPersistentData::FillData()
{
	for (auto& item : m_soldiersList)
	{
		if (item.m_type == ESoldierTypes::Infantry)
			item.m_image = m_infantryTexture;
		else if (item.m_type == ESoldierTypes::Sniper)
			item.m_image = m_sniperTexture;
		else if (item.m_type == ESoldierTypes::Heavy)
			item.m_image = m_heavyTexture;
		else if (item.m_type == ESoldierTypes::Medic)
			item.m_image = m_medicTexture;
	}
}

void UPersistentData::SoldierSelected(FSoldierInfo soldier)
{
	if (soldier.m_status == ESoldierStates::Dead || soldier.m_status == ESoldierStates::Empty)
		return;
	m_selectedSoldiers.AddUnique(soldier);
	PrintSelected();
}

void UPersistentData::SoldierDeselected(FSoldierInfo soldier)
{
	m_selectedSoldiers.Remove(soldier);
	PrintSelected();
}

void UPersistentData::PrintSelected()
{
	for (auto& item : m_selectedSoldiers)
	{
		UE_LOG(LogTemp, Warning, TEXT("abc %s"),*item.m_name);
	}
}

void UPersistentData::UpdateSoldiers(const TArray<ASoldier*>& list)
{
	for (auto& newSoldier : list)
	{
		if (newSoldier->GetHealthPercentage() < 0.5f && newSoldier->GetInfo().m_status != ESoldierStates::Dead)
			newSoldier->GetInfo().m_status = ESoldierStates::Injured;
		for (auto& oldSoldier : m_soldiersList)
		{
			if (oldSoldier.id == newSoldier->GetInfo().id)
			{
				oldSoldier.m_status = newSoldier->GetInfo().m_status;
			}
		}
	}

	//disable dead members
	for (int32 i = 0; i < m_soldiersList.Num(); i++)
	{
		SoldierDeselected(m_soldiersList[i]);
		if (m_soldiersList[i].m_status == ESoldierStates::Dead)
		{
			m_soldiersList.RemoveAt(i);
			i--;
		}
	}
	for (int32 i = 0; i < m_soldiersList.Num(); i++)
	{
		SoldierSelected(m_soldiersList[i]);
	}

}

void UPersistentData::MissionFinished(bool win)
{
	if (win)
		m_missionsSuccessful++;
	else m_missionsFailed++;
}

void UPersistentData::SoldierLost(FSoldierInfo info)
{
	m_soldiersLost++;

	for (auto& item : m_soldiersList)
	{
		if (item.id == info.id)
		{
			item.m_status = info.m_status;
			return;
		}
	}
}

FString UPersistentData::GetStateEnumString(ESoldierStates state)
{
	return soldierEnumToString::GetStateEnumString(state);
}

FString UPersistentData::GetClassEnumString(ESoldierTypes type)
{
	return soldierEnumToString::GetTypeEnumString(type);
}
