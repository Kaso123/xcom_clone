// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "CombatCamera.generated.h"

//This class represents player, his view on the battlefield and actions with his soldiers
UCLASS()
class XCOM_CLONE_API ACombatCamera : public ASpectatorPawn
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACombatCamera(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CameraDefaults)
	class UCameraComponent* m_cameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CameraDefaults)
	class USpringArmComponent* m_springArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_cameraMoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_cameraRotateSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_cameraZoomSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_startingArmLength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_minZoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraDefaults)
	float m_maxZoom;

	UPROPERTY(EditAnywhere, Category = "Points")
	int m_maxPoints = 20;

	UPROPERTY(EditAnywhere, Category = "SpawnPoints")
	TSubclassOf<class ASpawnPointPlayer> m_spawnPoint;

	UPROPERTY(EditAnywhere, Category = "SpawnPoints")
	TSubclassOf<class ACameraStart> m_cameraStartPoint;

	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeInfantry;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeHeavy;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeSniper;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeMedic;

	UPROPERTY(EditAnywhere, Category = "Selection")
	TSubclassOf<class ASelectionCircle> m_selectionCircle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	TArray<class ASoldier*> m_soldierList;
	class ASoldier* m_selectedUnit;
	class ASelectionCircle* m_selCircleInstance; //selection circle that is moved to selected soldier

	float m_forwardSpeed = 0.f;
	float m_rightSpeed = 0.f;
	float m_rotationSpeed = 0.f;
	float m_zoomSpeed = 0.f;

	void PositionCamera(float DeltaTime);
	void SpawnSoldiers(); //called on start of level

	bool m_bIsActiveTurn = false;
	int m_actionsInProgress = 0;

	int m_pointsLeft;

	void SpawnSelectionCircle(); //create selection circle (green underline)
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	bool IsWaiting(); //returns true when it is enemy's turn or animation is in progress

	UFUNCTION(BlueprintCallable)
	void EndTurnManually() { m_pointsLeft = 0; }; //when button to end turn is clicked

	//basic camera moving
	void MoveCameraForwardInput(float direction);
	void MoveCameraRightInput(float direction);
	void RotateCameraRightInput(float direction);
	void ZoomCameraInput(float direction);

	void StartTurn();
	void EndTurn();
	void LeaveMission();
	void Select(class ASoldier* soldier);
	void MoveToNode(class AGridNode* node);
	void Attack(class ASoldier* soldier);
	void RemoveSoldier(class ASoldier* soldier); //remove soldier from active list when killed
	
	void SaveSoldierStatsAfterMission();

	//used to wait for animations to finish
	void StartAction() { m_actionsInProgress++; }; 
	void FinishAction() { m_actionsInProgress--; };

	//strings for GUI
	UFUNCTION(BlueprintCallable)
	FString GetRemainingPointsString();
	UFUNCTION(BlueprintCallable)
	float GetRemainingPointsPercentage();

	class ASoldier* GetSelectedSoldier() { return m_selectedUnit; };
	const TArray<class ASoldier*>& GetSoldiersList() { return m_soldierList; };
};
