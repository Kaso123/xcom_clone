// Fill out your copyright notice in the Description page of Project Settings.

#include "GridNode.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Materials/Material.h"
#include "Math/Color.h"
#include "Materials/MaterialInstanceDynamic.h"


// Sets default values
AGridNode::AGridNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	m_rootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(m_rootComponent);
	m_groundMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GroundMesh"));
	m_boxComponent = CreateDefaultSubobject<UBoxComponent>("BoxComponent");

	m_matInstanceInterface = CreateDefaultSubobject<UMaterialInstanceDynamic>(TEXT("MaterialInstance"));
	
	m_boxComponent->SetHiddenInGame(false, true);
	
	m_boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AGridNode::OnOverlapBegin);

	m_coverInfo.Init(-1, 4);
}

// Called when the game starts or when spawned
void AGridNode::BeginPlay()
{
	Super::BeginPlay();
	m_boxComponent->AttachToComponent(m_rootComponent,FAttachmentTransformRules::KeepRelativeTransform,FName("BoxComp"));
	m_groundMesh->AttachToComponent(m_rootComponent, FAttachmentTransformRules::KeepRelativeTransform, FName("GroundMesh"));


	TArray<UPrimitiveComponent*> hits;
	m_boxComponent->GetOverlappingComponents(hits);
	m_matInstance = m_groundMesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0, m_matInstanceInterface);
	
	m_matInstance->SetVectorParameterValue("Color", hits.Num() > 0 ?
		FLinearColor(1.0f, 0.0f, 0.0f, 1.0f) /*red*/ : FLinearColor(0.0f, 0.0f, 1.0f, 1.0f) /*blue*/);
	m_matInstance->SetScalarParameterValue("Outline", 1.0f);

	if (hits.Num() > 0)
	{
		m_bIsWalkable = false;
		m_bCanSelectedWalkHere = false;
	}
	else
	{
		m_bIsWalkable = true;
		m_bCanSelectedWalkHere = true;
	}
}

void AGridNode::Init(int x, int y)
{
	m_coordinates.X = x;
	m_coordinates.Y = y;
}

// Called every frame
void AGridNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AGridNode::SelectionChanged()
{
	m_bCanSelectedWalkHere = false;
	m_distFromSelected = INT_MAX;
	m_coverAppealForEnemy = INT_MIN;
}

void AGridNode::ShowNode()
{
	if (m_bCanSelectedWalkHere)
	{
		m_matInstance->SetVectorParameterValue("Color", FLinearColor(0.0f, 0.0f, 1.0f, 1.0f));
		SetActorHiddenInGame(false);
	}
	
}

void AGridNode::HideNode()
{
	SetActorHiddenInGame(true);
	m_bCanSelectedWalkHere = false;
}

void AGridNode::MouseOverNode()
{
	m_matInstance->SetVectorParameterValue("Color", m_bCanSelectedWalkHere ? 
		FLinearColor(0.0f, 1.0f, 0.0f, 1.0f) /*green*/ : FLinearColor(1.0f, 0.0f, 0.0f, 1.0f) /*red*/);
	SetActorHiddenInGame(false);
}

void AGridNode::MouseOverNodeEnd()
{
	m_matInstance->SetVectorParameterValue("Color", m_bCanSelectedWalkHere ? 
		FLinearColor(0.0f, 0.0f, 1.0f, 1.0f) /*blue*/ : FLinearColor(1.0f, 0.0f, 0.0f, 1.0f) /*red*/);
}

void AGridNode::UpdateCoverGUI()
{
	m_matInstance->SetScalarParameterValue("ShieldRight", m_coverInfo[0] > 0 ? 1.0f : 0.0f);
	m_matInstance->SetScalarParameterValue("ShieldUp", m_coverInfo[1] > 0 ? 1.0f : 0.0f);
	m_matInstance->SetScalarParameterValue("ShieldLeft", m_coverInfo[2] > 0 ? 1.0f : 0.0f);
	m_matInstance->SetScalarParameterValue("ShieldDown", m_coverInfo[3] > 0 ? 1.0f : 0.0f);
}


void AGridNode::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	/*UE_LOG(LogTemp, Warning, TEXT("Overlap1"));
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlap2"));
		m_groundMesh->SetMaterial(0, m_unwalkableMat);
		m_bIsWalkable = false;
	}*/
}

