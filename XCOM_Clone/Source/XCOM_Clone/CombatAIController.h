// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CombatAIController.generated.h"

//used in Blueprint for Behaviour tree
UCLASS()
class XCOM_CLONE_API ACombatAIController : public AAIController
{
	GENERATED_BODY()
	
};
