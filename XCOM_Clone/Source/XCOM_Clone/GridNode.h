// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridNode.generated.h"

//represents one node on generated grid in level
UCLASS()
class XCOM_CLONE_API AGridNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridNode();
	void Init(int x, int y);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* m_groundMesh;

	UPROPERTY(EditAnywhere)
		class UMaterialInterface* m_matInstanceInterface;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool m_bIsWalkable = true;
	bool m_bIsOccupied = false;
	bool m_bCanSelectedWalkHere = false;
	int m_distFromSelected = INT_MAX;
	int m_coverAppealForEnemy = 0;
	bool m_isInRange = false;

	TArray<int> m_coverInfo;

	UMaterialInstanceDynamic* m_matInstance;

	FVector2D m_coordinates;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* m_rootComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* m_boxComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool IsOccupied() { return m_bIsOccupied; };
	bool IsWalkable() { return m_bIsWalkable; };
	bool CanSelectedWalkHere() { return m_bCanSelectedWalkHere; };
	
	void SetDistance(int dist) { m_distFromSelected = dist; };
	int GetDistance() { return m_distFromSelected; };

	void SetEnemyAppeal(int appeal) { m_coverAppealForEnemy = appeal; };
	int GetEnemyAppeal() { return m_coverAppealForEnemy; };

	void SelectionChanged();
	void SetCanWalkHere() { m_bCanSelectedWalkHere = true; };

	FVector2D GetCoordinates() { return m_coordinates; };
	void ShowNode();
	void HideNode();
	void MouseOverNode();
	void MouseOverNodeEnd();

	TArray<int>& GetCoverInfo() { return m_coverInfo; }; //shows when there is cover around this node
	void UpdateCoverGUI();

	void SetOccupied(bool value) { m_bIsOccupied = value; };
};



