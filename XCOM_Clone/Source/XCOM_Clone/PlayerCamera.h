// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "PlayerCamera.generated.h"

UCLASS()
class XCOM_CLONE_API APlayerCamera : public ASpectatorPawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerCamera(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class UCameraComponent* m_cameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float m_cameraMoveSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	float m_forwardSpeed = 0.f;
	float m_rightSpeed = 0.f;
	void PositionCamera();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void MoveCameraForwardInput(float direction);
	void MoveCameraRightInput(float direction);

};
