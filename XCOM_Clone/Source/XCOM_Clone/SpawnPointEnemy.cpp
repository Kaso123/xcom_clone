// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPointEnemy.h"

// Sets default values
ASpawnPointEnemy::ASpawnPointEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnPointEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnPointEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

