// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Soldier.h"
#include "BaseGameMode.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateSoldiersListDelegate, const TArray<FSoldierInfo>&, soldiersList);
/**
 * 
 */
UCLASS()
class XCOM_CLONE_API ABaseGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintAssignable, Category = "events")
	FUpdateSoldiersListDelegate delegate_onUpdateSoldiersList;


	UFUNCTION(BlueprintCallable)
	void BroadcastEvent();
};
