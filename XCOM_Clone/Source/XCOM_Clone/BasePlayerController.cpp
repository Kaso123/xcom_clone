// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePlayerController.h"
#include "Engine/World.h"
#include "PersistentData.h"

ABasePlayerController::ABasePlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
}
