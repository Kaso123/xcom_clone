// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatCamera.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Soldier.h"
#include "Engine/World.h"
#include "PersistentData.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "SpawnPointPlayer.h"
#include "CombatGameMode.h"
#include "CameraStart.h"
#include "SelectionCircle.h"
#include "GridNode.h"
#include "Engine/Classes/Components/SceneComponent.h"
#include "Engine/Classes/Components/InputComponent.h"


// Sets default values
ACombatCamera::ACombatCamera(const FObjectInitializer& ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	bAddDefaultMovementBindings = false; //disable WASD default

	// not needed Pitch Yaw Roll
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	m_cameraMoveSpeed = 2000.f;
	m_cameraRotateSpeed = 1000.f;
	m_cameraZoomSpeed = 2000.f;
	m_startingArmLength = 2000.f;
	m_minZoom = 200.0f;
	m_maxZoom = 4000.f;

	// intialize the camera
	m_springArmComponent = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArm"));
	m_springArmComponent->SetupAttachment(RootComponent);

	m_cameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Overhead Camera"));
	m_cameraComponent->SetupAttachment(m_springArmComponent);
	m_cameraComponent->bUsePawnControlRotation = false;
}

// Called when the game starts or when spawned
void ACombatCamera::BeginPlay()
{
	Super::BeginPlay();

	//place camera on desired spot in level
	AActor* start = UGameplayStatics::GetActorOfClass(GetWorld(), m_cameraStartPoint);
	check(start != nullptr);
	SetActorLocation(start->GetActorLocation());
	m_springArmComponent->TargetArmLength = m_startingArmLength;

	m_pointsLeft = m_maxPoints;

	SpawnSoldiers();
}

bool ACombatCamera::IsWaiting()
{
	if (!m_bIsActiveTurn)
		return true;

	if (m_actionsInProgress > 0)
		return true;

	return false;
}

// Called every frame
void ACombatCamera::Tick(float DeltaTime)
{
	//Super::Tick(DeltaTime);
	PositionCamera(DeltaTime);

	if (IsWaiting())
		return;

	//end turn if 0 points
	if (m_pointsLeft <= 0)
	{
		m_bIsActiveTurn = false;
		m_pointsLeft = m_maxPoints;
		ACombatGameMode* gm = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode());
		check(gm != nullptr);
		gm->EndPlayerTurn();
	}

}

void ACombatCamera::SpawnSoldiers()
{
	//spawn parameters
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	spawnParams.Instigator = this;

	UPersistentData* data = Cast<UPersistentData>(GetWorld()->GetGameInstance());

	check(data != nullptr);
	check(m_typeSniper != nullptr);
	check(m_typeInfantry != nullptr);
	check(m_typeHeavy != nullptr);
	check(m_typeMedic != nullptr);

	TArray<AActor*> spawnPoints; //points in level that were manually selected

	while (spawnPoints.Num() <= 0)
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), m_spawnPoint, spawnPoints);

	int lastSpawnPoint = 0;

	for (const auto& item : data->GetSelectedList())
	{
		TSubclassOf<ASoldier> typeToMake;
		if (item.m_type == ESoldierTypes::Infantry)
			typeToMake = m_typeInfantry;
		else if (item.m_type == ESoldierTypes::Sniper)
			typeToMake = m_typeSniper;
		else if (item.m_type == ESoldierTypes::Heavy)
			typeToMake = m_typeHeavy;
		else if (item.m_type == ESoldierTypes::Medic)
			typeToMake = m_typeMedic;
		else continue;

		if (lastSpawnPoint >= spawnPoints.Num())
			break;

		ASoldier* newSoldier = GetWorld()->SpawnActor<ASoldier>(typeToMake,
			FVector(spawnPoints[lastSpawnPoint]->GetActorLocation().X, spawnPoints[lastSpawnPoint]->GetActorLocation().Y, 100.f),
			FRotator(0.f, 0.f, 0.f), spawnParams);

		lastSpawnPoint++;
		newSoldier->SetInfo(item);
		newSoldier->SetIsEnemy(false);
		if (item.m_status == ESoldierStates::Injured) //for injured soldiers lower their health
			newSoldier->LowerHealth();
		m_soldierList.Add(newSoldier);
	}
	StartTurn();
}

void ACombatCamera::PositionCamera(float DeltaTime)
{
	
	FVector currLocation = GetActorLocation();
	FVector movement = FVector::ZeroVector;

	FRotator currRotation = GetActorRotation();
	FRotator rotation = FRotator::ZeroRotator;

	FRotator cameraYaw = FRotator(0.0f, m_cameraComponent->GetComponentToWorld().Rotator().Yaw, 0.0f);

	movement += m_forwardSpeed * m_cameraMoveSpeed * DeltaTime * cameraYaw.Vector();
	movement += m_rightSpeed * m_cameraMoveSpeed * DeltaTime * (FRotator(0.f, 90.f, 0.f) + cameraYaw).Vector();

	rotation.Yaw -= m_rotationSpeed * m_cameraRotateSpeed * DeltaTime;

	if (m_bIsActiveTurn) //only move camera when it is players turn
	{
		currLocation += movement;
	}
	else //on enemy turn, camera follows enemy soldiers
	{
		FVector desiredLoc = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetDesiredCameraPosition();
		if(desiredLoc.Z == 0.0f)
			currLocation = desiredLoc;
	}
	
	currRotation += rotation;
	SetActorLocation(currLocation);
	SetActorRotation(currRotation);

	float newArmLength = m_springArmComponent->TargetArmLength + m_zoomSpeed * m_cameraZoomSpeed * DeltaTime;
	m_springArmComponent->TargetArmLength = FMath::Clamp(newArmLength, m_minZoom, m_maxZoom);	
}

void ACombatCamera::StartTurn()
{
	m_bIsActiveTurn = true;
	m_pointsLeft = m_maxPoints;
	if (m_soldierList.Num() <= 0)
	{
		//GameOver
	}
	else //select soldier on start of the turn
	{
		Select(m_soldierList[0]);
		//Move camera to soldier
		SetActorLocation(FVector(m_soldierList[0]->GetActorLocation().X, m_soldierList[0]->GetActorLocation().Y, 0.0f));
		
	}
}

void ACombatCamera::EndTurn()
{
	m_bIsActiveTurn = false;
}

void ACombatCamera::LeaveMission()
{
	if (IsWaiting())
		return;

	while (m_soldierList.Num() > 0)
		m_soldierList[0]->Die();
	//m_soldierList.Empty();
	//Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GameOver(false);
}

void ACombatCamera::Select(ASoldier* soldier)
{
	if (IsWaiting())
		return;

	if (!m_selCircleInstance)
	{
		SpawnSelectionCircle();
	}

	if (m_selCircleInstance) //show selection on ground
	{
		m_selCircleInstance->SetActorLocation(FVector(soldier->GetActorLocation().X, soldier->GetActorLocation().Y, 0.0f));
		m_selCircleInstance->SetActorHiddenInGame(false);
	}

	soldier->Select(m_pointsLeft);
	m_selectedUnit = soldier;
}

void ACombatCamera::MoveToNode(AGridNode * node)
{
	if (IsWaiting())
		return;

	if (!m_selectedUnit)
		return;

	if (m_pointsLeft >= m_selectedUnit->GetMoveCost()*node->GetDistance())
	{
		if (node->CanSelectedWalkHere() && node->IsWalkable())
		{
			m_pointsLeft -= m_selectedUnit->GetMoveCost()*node->GetDistance();
			m_selectedUnit->StartMoving(node, m_pointsLeft);
			m_selCircleInstance->SetActorHiddenInGame(true);
			Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->UpdateHUDPoints();
		}
	}
	else
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->ShowMessageOnScreen(FString(TEXT("Not enough points!")));
}

void ACombatCamera::Attack(ASoldier * soldier)
{
	if (IsWaiting())
		return;

	if (!m_selectedUnit)
		return;

	if (!soldier->IsEnemy() || soldier->IsDying())
		return;

	if (m_selectedUnit->IsInRange(soldier))
	{
		if (m_pointsLeft >= m_selectedUnit->GetShotCost())
		{
			m_pointsLeft -= m_selectedUnit->GetShotCost();
			m_selectedUnit->Shoot(soldier, m_pointsLeft);
			Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->UpdateHUDPoints();
		}
		else
			Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->ShowMessageOnScreen(FString(TEXT("Not enough points!")));
	}
	else
	{
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->ShowMessageOnScreen(FString(TEXT("Target out of range!")));
	}
}

void ACombatCamera::RemoveSoldier(ASoldier * soldier)
{
	m_soldierList.Remove(soldier);

	if (m_soldierList.Num() <= 0)
	{
		//GameOver
		UE_LOG(LogTemp, Warning, TEXT("All soldiers are dead"));
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GameOver(false);
		return;
	}
}

void ACombatCamera::SaveSoldierStatsAfterMission()
{
	UPersistentData* data = Cast<UPersistentData>(GetWorld()->GetGameInstance());
	check(data != nullptr);

	data->UpdateSoldiers(m_soldierList);
}

FString ACombatCamera::GetRemainingPointsString()
{
	return FString::Printf(TEXT("Points: %d / %d"), m_pointsLeft, m_maxPoints);
}

float ACombatCamera::GetRemainingPointsPercentage()
{
	return (float)m_pointsLeft/(float)m_maxPoints;
}

void ACombatCamera::SpawnSelectionCircle()
{
	//spawn parameters
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	m_selCircleInstance = GetWorld()->SpawnActor<ASelectionCircle>(m_selectionCircle,
		FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f), spawnParams);
}

void ACombatCamera::MoveCameraForwardInput(float direction)
{
	m_forwardSpeed = direction;
}

void ACombatCamera::MoveCameraRightInput(float direction)
{
	m_rightSpeed = direction;
}

void ACombatCamera::RotateCameraRightInput(float direction)
{
	m_rotationSpeed = direction;
}

void ACombatCamera::ZoomCameraInput(float direction)
{
	m_zoomSpeed = direction;
}

