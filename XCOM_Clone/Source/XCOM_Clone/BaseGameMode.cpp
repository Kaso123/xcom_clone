// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseGameMode.h"
#include "Engine/World.h"
#include "PersistentData.h"

void ABaseGameMode::BeginPlay() {

}

void ABaseGameMode::BroadcastEvent()
{
	UPersistentData* data = Cast<UPersistentData>(GetWorld()->GetGameInstance());

	if (!data)
		_ASSERT(0);

	data->FillData();
	TArray<FSoldierInfo> &arr = data->GetSoldiersList();
	delegate_onUpdateSoldiersList.Broadcast(arr);
}
