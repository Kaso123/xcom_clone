// Fill out your copyright notice in the Description page of Project Settings.

#include "Soldier.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/GameFramework/Controller.h"
#include "AIController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameModeBase.h"
#include "Math/UnrealMathUtility.h"
#include "CombatGameMode.h"
#include "EnemyPawn.h"
#include "PersistentData.h"
#include "CombatCamera.h"
#include "GridNode.h"
#include "GridManager.h"


// Sets default values
ASoldier::ASoldier()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASoldier::BeginPlay()
{
	Super::BeginPlay();

	m_currentSpeed = 0.0f;
	m_aiCtrl = Cast<AAIController>(GetController());
	
	m_gridManager = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetGridManager();
	check(m_gridManager != nullptr);
	m_currentNode = m_gridManager->GetClosestGridNode(this->GetActorLocation());
	check(m_currentNode != nullptr);
	m_currentNode->SetOccupied(true);
}

bool ASoldier::GetRandomHitChance(ASoldier * target)
{
	int chanceToHit = m_statsAim - GetCoverValue(target);
	chanceToHit = FMath::Clamp(chanceToHit, 1, 99);

	int roll = FMath::RandRange(0, 100);
	ShowAttackStatsOnScreen(chanceToHit, GetCoverValue(target), chanceToHit >= roll);
	//UE_LOG(LogTemp, Warning, TEXT("chanceToHit: %d, Roll: %d"), chanceToHit,roll);
	if (chanceToHit < roll)
		return false;

	return true;

}

// Called every frame
void ASoldier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	check(m_aiCtrl != nullptr);

	SetSpeed(GetVelocity().Size2D());

	if (m_bIsMoving)
	{
		
		if (IsCloseToTargetNode())
		{
			m_bIsMoving = false;
			FinishActionInInstigator();
		}
		else if (m_currentSpeed < 10.0f)
		{
			m_moveStuckCounter++;
			if (m_moveStuckCounter > m_maxMoveStuckCounter)
				TeleportToTarget();
		}
		else
		{
			m_moveStuckCounter = 0;
		}

	}
	//UE_LOG(LogTemp, Warning, TEXT("speed: %f"), GetVelocity().Size());
	

}

void ASoldier::Select(int pointsLeft,bool bEnemy /*=false*/)
{
	check(m_gridManager != nullptr);

	m_gridManager->CalculateSoldierRange(this,pointsLeft,bEnemy);

	for (auto x : m_currentNode->GetCoverInfo())
		UE_LOG(LogTemp, Warning, TEXT("%d "), x);
}

void ASoldier::StartMoving(AGridNode* node, int pointsLeft)
{
	check(m_aiCtrl != nullptr);
	check(m_gridManager != nullptr);

	m_moveStuckCounter = 0;

	if (node)
	{
		m_aiCtrl->MoveToLocation(node->GetActorLocation(), 1.0f, false);
		m_gridManager->ClearNodes();

		m_currentNode->SetOccupied(false);
		node->SetOccupied(true);
		m_currentNode = node;
		
		StartActionInInstigator();
		m_bIsMoving = true;
		Select(pointsLeft);		
	}
}

void ASoldier::SetSpeed(float speed)
{
	m_currentSpeed = speed;
}

void ASoldier::TakeDmg(int dmg)
{
	UE_LOG(LogTemp, Warning, TEXT("taking dmg"));
	m_statsHealth -= dmg;

	

	if (m_statsHealth <= 0)
		Die();
	else
		m_bIsHit = true;
}

void ASoldier::Die()
{
	m_bIsDying = true;
	m_info.m_status = ESoldierStates::Dead;
	m_currentNode->SetOccupied(false);
	
	if (m_bIsEnemy)
	{
		Cast<UPersistentData>(GetGameInstance())->SoldierKilled();
		Cast<AEnemyPawn>(GetInstigator())->RemoveSoldier(this);
	}
	else
	{
		Cast<UPersistentData>(GetGameInstance())->SoldierLost(m_info);
		Cast<ACombatCamera>(GetInstigator())->RemoveSoldier(this);
	}
		
}


void ASoldier::Shoot(ASoldier * target, int pointsLeft)
{
	check(target != nullptr);

	FRotator newRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), target->GetActorLocation());
	SetActorRotation(newRotation);

	m_bIsShooting = true;
	Select(pointsLeft);

	if (GetRandomHitChance(target))
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit!"));
		target->TakeDmg(m_statsDamage);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Miss!"));
	}

}

void ASoldier::LowerHealth()
{
	m_statsHealth = m_statsMaxHealth / 2;
}

AGridNode * ASoldier::GetNode()
{
	return m_currentNode;
}

bool ASoldier::IsFriendly(ASoldier * soldier)
{
	return m_bIsEnemy == soldier->IsEnemy();
}

bool ASoldier::IsInRange(ASoldier * soldier)
{
	return IsInRange(soldier->GetNode());
}

bool ASoldier::IsInRange(AGridNode * target)
{
	//subtract vectors and get length
	FVector sub = target->GetActorLocation() - m_currentNode->GetActorLocation();

	if (sub.Size() > m_statsRange * 100.0f)
		return false;
	return true;
}

void ASoldier::StopShootingAnim()
{
	m_bIsShooting = false;
}

void ASoldier::StopHitAnim()
{
	m_bIsHit = false;
}

int ASoldier::GetCoverValue(ASoldier* target)
{
	return GetCoverValue(target->GetNode());
}

int ASoldier::GetCoverValue(AGridNode * target)
{
	FVector2D targetCoord = target->GetCoordinates();
	FVector2D myCoord = m_currentNode->GetCoordinates();

	if (targetCoord.X < myCoord.X && target->GetCoverInfo()[0] > 0)
		return 40;
	if (targetCoord.Y > myCoord.Y && target->GetCoverInfo()[1] > 0)
		return 40;
	if (targetCoord.X > myCoord.X && target->GetCoverInfo()[2] > 0)
		return 40;
	if (targetCoord.Y < myCoord.Y && target->GetCoverInfo()[3] > 0)
		return 40;

	return 0;
}

void ASoldier::TeleportToTarget()
{
	SetActorLocation(FVector(m_currentNode->GetActorLocation().X, m_currentNode->GetActorLocation().Y,GetActorLocation().Z));
	m_moveStuckCounter = 0;
}

void ASoldier::ShowAttackStatsOnScreen(int chance, int cover, bool hit)
{
	if(hit)
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->ShowMessageOnScreen(FString::Printf(TEXT("%s dealt %d damage. Chance to hit: %d(-%d)%"),
			*m_info.m_name,m_statsDamage,chance,cover));
	else 
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->ShowMessageOnScreen(FString::Printf(TEXT("%s missed shot. Chance to hit: %d(-%d)%"),
			*m_info.m_name,chance,cover));
}

void ASoldier::StartActionInInstigator()
{
	if (m_bIsEnemy)
	{
		AEnemyPawn* inst = Cast<AEnemyPawn>(GetInstigator());
		check(inst != nullptr);
		inst->StartAction();
	}
	else
	{
		ACombatCamera* inst = Cast<ACombatCamera>(GetInstigator());
		check(inst != nullptr);
		inst->StartAction();
	}
}

void ASoldier::FinishActionInInstigator()
{
	if (m_bIsEnemy)
	{
		AEnemyPawn* inst = Cast<AEnemyPawn>(GetInstigator());
		check(inst != nullptr);
		inst->FinishAction();
	}
	else
	{
		ACombatCamera* inst = Cast<ACombatCamera>(GetInstigator());
		check(inst != nullptr);
		inst->FinishAction();
	}
}

bool ASoldier::IsCloseToTargetNode()
{
	return (FMath::Abs(m_currentNode->GetActorLocation().X - GetActorLocation().X) < 25.0f) &&
		(FMath::Abs(m_currentNode->GetActorLocation().Y - GetActorLocation().Y) < 25.0f);
}

FString ASoldier::GetClassString()
{
	return soldierEnumToString::GetTypeEnumString(m_info.m_type);
}

void ASoldier::ShowHoverGUI_Implementation()
{
}

void ASoldier::HideHoverGUI_Implementation()
{
}

void ASoldier::ShowRangeGUI_Implementation()
{
}

void ASoldier::HideRangeGUI_Implementation()
{
}

// Called to bind functionality to input
void ASoldier::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FString soldierEnumToString::GetTypeEnumString(ESoldierTypes type)
{
	switch (type) {
	case ESoldierTypes::Infantry:
		return FString(TEXT("Infantry"));
		break;
	case ESoldierTypes::Heavy:
		return FString(TEXT("Heavy"));
		break;
	case ESoldierTypes::Medic:
		return FString(TEXT("Medic"));
		break;
	case ESoldierTypes::Sniper:
		return FString(TEXT("Sniper"));
		break;
	default:
		break;
	}

	return FString(TEXT("Unknown"));
}

FString soldierEnumToString::GetStateEnumString(ESoldierStates state)
{
	switch (state) {
	case ESoldierStates::Dead:
		return FString(TEXT("Dead"));
		break;
	case ESoldierStates::Injured:
		return FString(TEXT("Injured"));
		break;
	case ESoldierStates::Ready:
		return FString(TEXT("Ready"));
		break;
	default:
		break;
	}

	return FString(TEXT("Unknown"));
}