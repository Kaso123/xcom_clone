// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyPawn.h"
#include "Engine\World.h"
#include "Kismet/GameplayStatics.h"
#include "Soldier.h"
#include "PersistentData.h"
#include "SpawnPointEnemy.h"
#include "GridManager.h"
#include "CombatCamera.h"
#include "GridNode.h"
#include "CombatGameMode.h"

// Sets default values
AEnemyPawn::AEnemyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
}

// Called when the game starts or when spawned
void AEnemyPawn::BeginPlay()
{
	Super::BeginPlay();

	m_pointsLeft = m_maxPoints;
	//spawn parameters
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	spawnParams.Instigator = this;

	UPersistentData* data = Cast<UPersistentData>(GetWorld()->GetGameInstance());

	check(data != nullptr);
	check(m_typeSniper != nullptr);
	check(m_typeInfantry != nullptr);
	check(m_typeHeavy != nullptr);
	check(m_typeMedic != nullptr);

	//spawning enemy soldiers
	TArray<AActor*> spawnPoints;

	while (spawnPoints.Num() <= 0)
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), m_spawnPoint, spawnPoints);

	int lastSpawnPoint = 0;

	for (const auto& item : data->GetEnemiesList())
	{
		TSubclassOf<ASoldier> typeToMake;
		if (item.m_type == ESoldierTypes::Infantry)
			typeToMake = m_typeInfantry;
		else if (item.m_type == ESoldierTypes::Sniper)
			typeToMake = m_typeSniper;
		else if (item.m_type == ESoldierTypes::Heavy)
			typeToMake = m_typeHeavy;
		else if (item.m_type == ESoldierTypes::Medic)
			typeToMake = m_typeMedic;
		else continue;

		if (lastSpawnPoint >= spawnPoints.Num())
			break;

		ASoldier* newSoldier = GetWorld()->SpawnActor<ASoldier>(typeToMake,
			FVector(spawnPoints[lastSpawnPoint]->GetActorLocation().X, spawnPoints[lastSpawnPoint]->GetActorLocation().Y, 100.f),
			FRotator(0.f, 0.f, 0.f), spawnParams);

		lastSpawnPoint++;
		newSoldier->SetInfo(item);
		newSoldier->SetIsEnemy(true);
		m_soldierList.Add(newSoldier);
	}
	
}

bool AEnemyPawn::HasGoodCover(ASoldier * soldier)
{
	const TArray<ASoldier*>& playerSoldiers = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetPlayer()->GetSoldiersList();

	for (auto attacker : playerSoldiers)
	{
		if (attacker->IsInRange(soldier) && attacker->GetCoverValue(soldier) <= 0)
			return false;
	}
	return true;
}

ASoldier * AEnemyPawn::FindEnemyInRange(ASoldier * soldier)
{
	const TArray<ASoldier*>& playerSoldiers = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetPlayer()->GetSoldiersList();

	for (auto target : playerSoldiers)
	{
		if (soldier->IsInRange(target)) 
			return target; //returns first found enemy, not closest
	}
	return nullptr;
}

ASoldier * AEnemyPawn::FindClosestEnemy(ASoldier * soldier)
{
	const TArray<ASoldier*>& playerSoldiers = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GetPlayer()->GetSoldiersList();
	ASoldier* result = playerSoldiers[0];
	int minDist = GetDistanceBetweenSoldiers(soldier, result);

	for (auto target : playerSoldiers)
	{
		if (GetDistanceBetweenSoldiers(soldier, target) < minDist)
		{
			result = target;
			minDist = GetDistanceBetweenSoldiers(soldier, target);
		}		
	}
	
	return result;
}

int AEnemyPawn::GetDistanceBetweenSoldiers(ASoldier * soldier1, ASoldier * soldier2)
{
	return FMath::Abs(soldier1->GetNode()->GetCoordinates().X - soldier2->GetNode()->GetCoordinates().X) + 
		FMath::Abs(soldier1->GetNode()->GetCoordinates().Y - soldier2->GetNode()->GetCoordinates().Y);
}

// Called every frame
void AEnemyPawn::Tick(float DeltaTime)
{
	
	Super::Tick(DeltaTime);

	//this is not used anymore, behaviour tree is used
	/*
	if (IsWaiting())
		return;
	
	if (m_pointsLeft > 0)
	{
		//TestFunction();
	}
	else
	{
		m_bIsActiveTurn = false;
		m_selectedSoldier = nullptr;
		m_pointsLeft = m_maxPoints;
		ACombatGameMode* gm = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode());
		_ASSERT(gm);
		gm->EndEnemyTurn();
	}*/

}

void AEnemyPawn::StartTurn()
{
	m_bIsActiveTurn = true;
}

void AEnemyPawn::EndTurn()
{
	m_bIsActiveTurn = false;
	m_selectedSoldier = nullptr;
	m_pointsLeft = m_maxPoints;
	ACombatGameMode* gm = Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode());
	_ASSERT(gm);
	gm->EndEnemyTurn();
}

void AEnemyPawn::MakeAIMove()
{
	AGridManager* gridMan = Cast<AGridManager>(UGameplayStatics::GetActorOfClass(GetWorld(), m_gridManagerClass));
	check(gridMan != nullptr);

	//select random soldier
	ASoldier* randomSoldier = m_soldierList[FMath::RandRange(0, m_soldierList.Num() - 1)];
	m_selectedSoldier = randomSoldier;
	m_selectedSoldier->Select(FMath::Clamp(m_pointsLeft,0,m_maxPointsForOneMove),true);

	if (HasGoodCover(m_selectedSoldier)) //is not exposed to enemies
	{
		if (m_pointsLeft >= m_selectedSoldier->GetShotCost()) //has enough points to shoot
		{
			ASoldier* target = FindEnemyInRange(m_selectedSoldier);
			if (target)
			{
				//shoot
				m_pointsLeft -= m_selectedSoldier->GetShotCost();
				m_selectedSoldier->Shoot(target, m_pointsLeft);
			}
			else
			{
				//move to random location towards random enemy
				AGridNode* nodeTarget = gridMan->GetBestAppealTowardsEnemy(m_selectedSoldier, 
					FMath::Clamp(m_pointsLeft, 0, m_maxPointsForOneMove),FindClosestEnemy(m_selectedSoldier));

				UE_LOG(LogTemp, Warning, TEXT("target appeal: %d"), nodeTarget->GetEnemyAppeal());
				if (nodeTarget && nodeTarget != m_selectedSoldier->GetNode())
				{
					m_pointsLeft -= m_selectedSoldier->GetMoveCost()*nodeTarget->GetDistance();
					m_selectedSoldier->StartMoving(nodeTarget, 0);
				}
				else
				{
					EndTurn();
				}
			}
		}
		else //not enough points to shoot
		{
			EndTurn();
		}
	}
	else //is exposed to enemies
	{
		//move to cover
		AGridNode* target = gridMan->GetBestAppeal(m_selectedSoldier, FMath::Clamp(m_pointsLeft, 0, m_maxPointsForOneMove));
		if (target && target != m_selectedSoldier->GetNode())
		{
			m_pointsLeft -= m_selectedSoldier->GetMoveCost()*target->GetDistance();
			m_selectedSoldier->StartMoving(target, 0);	
		}
		else
		{
			EndTurn();
		}		
	}

	Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->UpdateHUDPoints();
}

void AEnemyPawn::RemoveSoldier(ASoldier * soldier)
{
	m_soldierList.Remove(soldier);

	if (m_soldierList.Num() <= 0)
	{
		//GameOver
		UE_LOG(LogTemp, Warning, TEXT("All enemies are dead"));
		Cast<ACombatGameMode>(GetWorld()->GetAuthGameMode())->GameOver(true); //player won
		return;
	}
}

FVector AEnemyPawn::GetSelectedLocation()
{
	if (!m_selectedSoldier)
		return FVector(0.0f, 0.0f, -1.0f); //if -1 is returned on Z, camera doesnt move
	else
		return FVector(m_selectedSoldier->GetActorLocation().X, m_selectedSoldier->GetActorLocation().Y, 0.0f);
}

bool AEnemyPawn::IsWaiting()
{
	if (!m_bIsActiveTurn)
		return true;

	if (m_actionsInProgress > 0)
		return true;

	return false;
}

FString AEnemyPawn::GetRemainingPointsString()
{
	return FString::Printf(TEXT("Points: %d / %d"), m_pointsLeft, m_maxPoints);
}

float AEnemyPawn::GetRemainingPointsPercentage()
{
	return (float)m_pointsLeft / (float)m_maxPoints;
}


// Called to bind functionality to input
void AEnemyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

