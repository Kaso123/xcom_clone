// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "EnemyPawn.generated.h"


//this is basic enemy pawn that is controlling all enemy soldiers
UCLASS()
class XCOM_CLONE_API AEnemyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool m_bIsActiveTurn = false;
	int m_actionsInProgress = 0;

	int m_pointsLeft;

	UPROPERTY(EditAnywhere, Category = "Points")
	int m_maxPoints = 20;
	UPROPERTY(EditAnywhere, Category = "Points")
	int m_maxPointsForOneMove = 8;

	TArray<class ASoldier*> m_soldierList;
	class ASoldier* m_selectedSoldier;

	bool HasGoodCover(class ASoldier* soldier); //checks if any enemy can hit this soldier without cover
	class ASoldier* FindEnemyInRange(class ASoldier* soldier); //checks if there is enemy in range
	class ASoldier* FindClosestEnemy(class ASoldier* soldier); 
	int GetDistanceBetweenSoldiers(class ASoldier* soldier1, class ASoldier* soldier2);

public:

	UPROPERTY(EditAnywhere, Category = "SpawnPoints")
	TSubclassOf<class ASpawnPointEnemy> m_spawnPoint;

	UPROPERTY(EditAnywhere, Category = "SpawnPoints")
	TSubclassOf<class AGridManager> m_gridManagerClass;

	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeInfantry;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeHeavy;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeSniper;
	UPROPERTY(EditAnywhere, Category = "Soldiers")
	TSubclassOf<class ASoldier> m_typeMedic;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void StartTurn();

	UFUNCTION(BlueprintCallable)
	void EndTurn();

	UFUNCTION(BlueprintCallable)
	void MakeAIMove(); //do something, move or shoot
	void SaveSoldierStatsAfterMission() { return; };

	//used for waiting for animation finish
	void StartAction() { m_actionsInProgress++; };
	void FinishAction() { m_actionsInProgress--; };

	void RemoveSoldier(class ASoldier* soldier);

	FVector GetSelectedLocation(); //returns location of currently used soldier

	UFUNCTION(BlueprintCallable)
	bool IsWaiting();

	UFUNCTION(BlueprintCallable)
	FString GetRemainingPointsString();
	UFUNCTION(BlueprintCallable)
	float GetRemainingPointsPercentage();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
